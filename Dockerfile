FROM ubuntu:20.04
RUN apt-get update && apt-get install -y --no-install-recommends git
# RUN apt-get -y install wget git gcc libpq-dev python-dev python-pip python3 python3.8 python3.8-venv python3.8-dev python3-dev python3-pip python3-venv python3-wheel libpng-dev libfreetype6-dev libblas3 liblapack3 libblas-dev liblapack-dev pkg-config
RUN apt-get install -y python3 python3-pip

COPY . /tmp/INSTALLER

#RUN python3.8 -m venv my_env
#RUN source my_env/bin/activate


# RUN mkdir -p /tmp/INSTALLER

#RUN cd /tmp/INSTALLER
#RUN git clone https://gitlab.com/zbian4/noisifier.git
#RUN cd noisifier
#RUN pip install .

RUN pip install /tmp/INSTALLER && rm -rf /tmp/INSTALLER

ENTRYPOINT ["run-noisifier"]
