from setuptools import setup, find_packages

__package_name__ = "noisifier"


def get_version_and_cmdclass(pkg_path):
    """Load version.py module without importing the whole package.

    Template code from miniver
    """
    import os
    from importlib.util import module_from_spec, spec_from_file_location

    spec = spec_from_file_location("version", os.path.join(pkg_path, "_version.py"))
    module = module_from_spec(spec)
    spec.loader.exec_module(module)
    return module.__version__, module.get_cmdclass(pkg_path)


__version__, cmdclass = get_version_and_cmdclass(__package_name__)


# noinspection PyTypeChecker
setup(
    name=__package_name__,
    version="v0.0.1",
    description="noisifier: add noise to nii images",
    long_description="noisifier: add noise to nii images",
    author="ZB",
    author_email="xxx@jhu.edu",
    url="https://gitlab.com/zbian4/noisifier",
    license="Apache License, 2.0",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: Apache Software License",
        "Programming Language :: Python :: 3.10",
        "Topic :: Scientific/Engineering",
    ],
    packages=find_packages(),
    keywords="mri ct super-resolution",
    entry_points={
        "console_scripts": [
            "run-noisifier=noisifier.main:main",
        ]
    },
    install_requires=[
        "nibabel",
        "numpy",
    ],
    cmdclass=cmdclass,
)
