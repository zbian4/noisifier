# -*- coding: utf-8 -*-
# This file is part of 'miniver': https://github.com/jbweston/miniver
#
# This file will be overwritten by setup.py when a source or binary
# distribution is made.  The magic value "__use_git__" is interpreted by
# version.py.

version = "__use_git__"

# These values are only set if the distribution was created with 'git archive'
refnames = "HEAD -> main, tag: v4.0.5, refs/keep-around/f509e0da58237397e5e35af1d92b42f26eb06d2c"
git_hash = "f509e0d"
