import argparse
import nibabel as nib
import numpy as np
from pathlib import Path
import sys
import time


def noisify(x, mu, sigma):
    n = np.random.normal(mu, sigma, size=x.shape)
    return x + n


def main(args=None):
    st_time = time.time()
    text_div = "=" * 10

    parser = argparse.ArgumentParser()

    parser.add_argument("--in-fpath", type=str, required=True)
    parser.add_argument("--out-fpath", type=str, required=True)
    parser.add_argument("--mean", type=float, required=True)
    parser.add_argument("--std", type=float, required=True)

    args = parser.parse_args(args if args is not None else sys.argv[1:])

    in_fpath = Path(args.in_fpath).resolve()
    out_fpath = Path(args.out_fpath).resolve()

    print(text_div, "Loading image...", text_div)
    x_obj = nib.load(in_fpath)
    x = x_obj.get_fdata(dtype=np.float32)
    print(
        text_div,
        f"Adding noise of mean {args.mean:.2f} and std {args.std:.2f}...",
        text_div,
    )
    y = noisify(x, args.mean, args.std)
    y_obj = nib.Nifti1Image(y, affine=x_obj.affine, header=x_obj.header)
    print(text_div, f"Writing to disk...", text_div)
    nib.save(y_obj, out_fpath)
    en_time = time.time()
    print(text_div, f"DONE", text_div)
    print(f"\tElapsed time: {en_time - st_time:.2f}")


if __name__ == "__main__":
    main()
